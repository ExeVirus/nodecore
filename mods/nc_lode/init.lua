-- LUALOCALS < ---------------------------------------------------------
local include, nodecore
    = include, nodecore
-- LUALOCALS > ---------------------------------------------------------

nodecore.amcoremod()

include("ore")
include("metallurgy")
include("oresmelt")
include("tools")
include("shafts")
include("adze")
include("rake")
include("shelf")
include("hints")
