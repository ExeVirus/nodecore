-- LUALOCALS < ---------------------------------------------------------
local include, nodecore
    = include, nodecore
-- LUALOCALS > ---------------------------------------------------------

nodecore.amcoremod()

include("operate")
include("register")
include("ablation")
include("craft_catapult")
include("craft_press")
include("hints")
