msgid ""
msgstr ""
"PO-Revision-Date: 2020-01-12 16:26+0000\n"
"Last-Translator: Weblate Admin <warr1024@gmail.com>\n"
"Language-Team: Japanese <http://nodecore.mine.nu/trans/projects/nodecore/"
"core/ja/>\n"
"Language: ja\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: Weblate 3.8\n"

msgid "(and @1 more hints)"
msgstr "(さらに@1つのヒント)"

msgid "(and 1 more hint)"
msgstr "(さらに1つのヒント)"

msgid "(C)2018-2019 by Aaron Suen <warr1024@@gmail.com>"
msgstr "(C)2018-2019 から Aaron Suen <warr1024@@gmail.com>"

msgid "(C)2018-2020 by Aaron Suen <warr1024@@gmail.com>"
msgstr "(C)2018-2020 から Aaron Suen <warr1024@@gmail.com>"
