**NodeCore** is a major reconstruction of "survival" gameplay from the ground up.  *Relearn much of what you thought you knew about Minetest!*

#### Features

- Immersive gameplay.  Zero in-game pop-up GUIs.  Minimal HUDs. All interactions take place in continuous first-person perspective. Nothing gets between you and the game world.
- Nearly everything is on the grid. Item stacks settle onto the grid and persist.  Complex machines (e.g. furnaces, storage) are built in-world and can be designed, customized, optimized.
- In-world crafting.  Assemble more complex recipes directly on the grid or in-place.  Learn how to "pummel" nodes to hammer things together or break things apart.
- Built-in player guide and hint system provides low-spoiler guidance.
- Rich and subtle interactions.  Some materials dig and transform in stages. Some materials slump into repose when stacked high.  Some things transform if conditions are met, but transform faster based on environment.  Experiment and see what effects what!
- Build complex in-game systems.  Design your own furnace.  Construct digital logic circuits.  Build gears and rotary machinery.  Assemble portable storage containers.
- Eggcorns!

Note that NodeCore is a _complete_ game, but new features are still actively developed.  If you're looking for the latest experimental stuff, and don't mind a little instability or a few bugs, check out the [NodeCore ALPHA](/packages/Warr1024/nodecore_alpha/) early-access releases.

Join the Discord [https://discord.gg/NNYeF6f](https://discord.gg/NNYeF6f) or IRC at **#nodecore** on [freenode](https://freenode.net/)